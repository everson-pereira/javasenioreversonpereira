package br.com.eversoftware.mobicare.api.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Setor implements Serializable{
	
	private static final long serialVersionUID = 5330028144960024569L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;	
		
	private String descricao;
	
	@OneToMany(mappedBy = "setor", fetch = FetchType.EAGER)
    private List<Colaborador> colaboradores;
	
	public Setor() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public List<Colaborador> getColaboradores() {
		return colaboradores;
	}

	public void setColaboradores(List<Colaborador> colaboradores) {
		this.colaboradores = colaboradores;
	}

	@Override
	public String toString() {
		return "Setor [id=" + id + ", descricao=" + descricao + "]";
	}

}
