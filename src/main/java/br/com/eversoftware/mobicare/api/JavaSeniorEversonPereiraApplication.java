package br.com.eversoftware.mobicare.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class JavaSeniorEversonPereiraApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSeniorEversonPereiraApplication.class, args);
	}
}
