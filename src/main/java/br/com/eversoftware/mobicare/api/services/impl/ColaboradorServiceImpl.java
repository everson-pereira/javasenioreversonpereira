package br.com.eversoftware.mobicare.api.services.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.eversoftware.mobicare.api.entities.Colaborador;
import br.com.eversoftware.mobicare.api.repositories.ColaboradorRepository;
import br.com.eversoftware.mobicare.api.services.ColaboradorService;

@Service
public class ColaboradorServiceImpl implements ColaboradorService {
	
	private static final Logger log = LoggerFactory.getLogger(ColaboradorServiceImpl.class);

	@Autowired
	private ColaboradorRepository colaboradorRepository;
	
	public Colaborador persistir(Colaborador colaborador) {
		log.info("Persistindo colaborador: {}", colaborador);
		return this.colaboradorRepository.save(colaborador);
	}
	
	public Optional<Colaborador> buscarPorCpf(String cpf) {
		log.info("Buscando colaborador pelo CPF {}", cpf);
		return Optional.ofNullable(this.colaboradorRepository.findByCpf(cpf));
	}
	
	public Optional<Colaborador> buscarPorEmail(String email) {
		log.info("Buscando colaborador pelo email {}", email);
		return Optional.ofNullable(this.colaboradorRepository.findByEmail(email));
	}
	
	public Optional<Colaborador> buscarPorId(Long id) {
		log.info("Buscando colaborador pelo IDl {}", id);
		return this.colaboradorRepository.findById(id);
	}
	
	public void remover(Long id) {
		log.info("Removendo colaborador ID {}", id);
		this.colaboradorRepository.deleteById(id);
	}

}
