package br.com.eversoftware.mobicare.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eversoftware.mobicare.api.entities.Colaborador;

public interface ColaboradorRepository extends JpaRepository<Colaborador, Long>{
	
	Colaborador findByCpf(String cpf);
	
	Colaborador findByEmail(String email);

}
