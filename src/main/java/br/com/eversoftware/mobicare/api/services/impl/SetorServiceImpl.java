package br.com.eversoftware.mobicare.api.services.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import br.com.eversoftware.mobicare.api.entities.Setor;
import br.com.eversoftware.mobicare.api.repositories.SetorRepository;
import br.com.eversoftware.mobicare.api.services.SetorService;

@Service
public class SetorServiceImpl implements SetorService {

	private static final Logger log = LoggerFactory.getLogger(SetorServiceImpl.class);

	@Autowired
	private SetorRepository setorRepository;

	@Override
	public Optional<Setor> buscarPorId(Long id) {
		log.info("Buscando um Setor pelo ID {}", id);
		return setorRepository.findById(id);
	}
	
	@Override
	@Cacheable("setoresColaboradores")
	public Page<Setor> buscarComColaboradores(PageRequest pageRequest){
		log.info("Buscando todos Setores com Colaboradores");
		return setorRepository.findAllPageable(pageRequest);
	}

}
