package br.com.eversoftware.mobicare.api.dtos;

import java.util.List;

public class SetorDto {
	
	private Long id;
	private String descricao;
	private List<ColaboradorSimplesDto> colaboradores;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public List<ColaboradorSimplesDto> getColaboradores() {
		return colaboradores;
	}
	public void setColaboradores(List<ColaboradorSimplesDto> colaboradores) {
		this.colaboradores = colaboradores;
	}

}
