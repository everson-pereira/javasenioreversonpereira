package br.com.eversoftware.mobicare.api.services;

import java.util.Optional;

import br.com.eversoftware.mobicare.api.entities.Colaborador;

public interface ColaboradorService {
	
	/**
	 * Persiste um colaborador na base de dados.
	 * 
	 * @param colaborador
	 * @return Colaborador
	 */
	Colaborador persistir(Colaborador colaborador);
	
	/**
	 * Busca e retorna um colaborador dado um CPF.
	 * 
	 * @param cpf
	 * @return Optional<Colaborador>
	 */
	Optional<Colaborador> buscarPorCpf(String cpf);
	
	/**
	 * Busca e retorna um colaborador dado um email.
	 * 
	 * @param email
	 * @return Optional<Colaborador>
	 */
	Optional<Colaborador> buscarPorEmail(String email);
	
	/**
	 * Busca e retorna um colaborador por ID.
	 * 
	 * @param id
	 * @return Optional<Colaborador>
	 */
	Optional<Colaborador> buscarPorId(Long id);
	
	/**
	 * Remove um colaborador da base de dados.
	 * 
	 * @param id
	 */
	void remover(Long id);

}
