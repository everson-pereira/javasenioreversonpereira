package br.com.eversoftware.mobicare.api.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import br.com.eversoftware.mobicare.api.entities.Setor;

public interface SetorService {

	/**
	 * Retorna um setor por ID.
	 * 
	 * @param cnpj
	 * @return Optional<Setor>
	 */
	Optional<Setor> buscarPorId(Long id);
	
	/**
	 * Retorna todos os setor com seus colaboradores (nome e email) paginados.
	 * 
	 * @param pageRequest
	 * @return Page<Setor>
	 */	
	Page<Setor> buscarComColaboradores(PageRequest pageRequest);
	
	
}
