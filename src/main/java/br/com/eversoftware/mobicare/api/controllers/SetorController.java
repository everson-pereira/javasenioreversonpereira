package br.com.eversoftware.mobicare.api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.eversoftware.mobicare.api.dtos.ColaboradorSimplesDto;
import br.com.eversoftware.mobicare.api.dtos.SetorDto;
import br.com.eversoftware.mobicare.api.entities.Colaborador;
import br.com.eversoftware.mobicare.api.entities.Setor;
import br.com.eversoftware.mobicare.api.response.Response;
import br.com.eversoftware.mobicare.api.services.SetorService;

@RestController
public class SetorController {

	private static final Logger log = LoggerFactory.getLogger(SetorController.class);	
	
	@Autowired
	private SetorService setorService;
		
	@GetMapping(value = "/setores/colaboradores")
	public ResponseEntity<Response<Page<SetorDto>>> buscar(
			@RequestParam(value = "pag", defaultValue = "0") int pag,			
			@RequestParam(value = "qt", defaultValue = "10") int qt,
			@RequestParam(value = "ord", defaultValue = "id") String ord,
			@RequestParam(value = "dir", defaultValue = "DESC") String dir)	{	
		
		log.info("Buscando setores: ");
				
		Response<Page<SetorDto>> response = new Response<Page<SetorDto>>();		
		
		Page<Setor> setores = this.setorService.buscarComColaboradores(PageRequest.of(pag, qt, Direction.valueOf(dir), ord));
		Page<SetorDto> setoresDto = setores.map(setor -> this.converterSetorDto(setor));

		response.setData(setoresDto);
		return ResponseEntity.ok(response);
	}

	private SetorDto converterSetorDto(Setor setor) {

		SetorDto setorDto = new SetorDto();
		setorDto.setId(setor.getId());
		setorDto.setDescricao(setor.getDescricao());
		
		List<ColaboradorSimplesDto> colaboradoresSimplesDto = new ArrayList<ColaboradorSimplesDto>();
		for (Colaborador temp : setor.getColaboradores()) {
			colaboradoresSimplesDto.add(this.converterColaboradorSimplesDto(temp));			
		}
		
		setorDto.setColaboradores(colaboradoresSimplesDto);
		return setorDto;
	}

	private ColaboradorSimplesDto converterColaboradorSimplesDto(Colaborador colaborador) {
		
		ColaboradorSimplesDto colaboradorSimplesDto = new ColaboradorSimplesDto();
		colaboradorSimplesDto.setNome(colaborador.getNome());
		colaboradorSimplesDto.setEmail(colaborador.getEmail());				
		
		return colaboradorSimplesDto;
	}

}
