package br.com.eversoftware.mobicare.api.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.eversoftware.mobicare.api.entities.Setor;

public interface SetorRepository extends JpaRepository<Setor, Long> {
	
	@Query(value = "SELECT s FROM Setor s")
	Page<Setor> findAllPageable(Pageable pageable);
	
}
