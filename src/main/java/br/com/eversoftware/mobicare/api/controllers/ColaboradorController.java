package br.com.eversoftware.mobicare.api.controllers;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.eversoftware.mobicare.api.dtos.ColaboradorDto;
import br.com.eversoftware.mobicare.api.entities.Colaborador;
import br.com.eversoftware.mobicare.api.entities.Setor;
import br.com.eversoftware.mobicare.api.response.Response;
import br.com.eversoftware.mobicare.api.services.ColaboradorService;
import br.com.eversoftware.mobicare.api.services.SetorService;

@RestController
public class ColaboradorController {

	private static final Logger log = LoggerFactory.getLogger(ColaboradorController.class);

	@Autowired
	private ColaboradorService colaboradorService;
	
	@Autowired
	private SetorService setorService;

	@PostMapping("/colaboradores")
	public ResponseEntity<Response<ColaboradorDto>> cadastrar(@Valid @RequestBody ColaboradorDto colaboradorDto, BindingResult result) throws NoSuchAlgorithmException {
		
		log.info("Cadastrando Colaborador: {}", colaboradorDto.toString());
		Response<ColaboradorDto> response = new Response<ColaboradorDto>();

		validarDadosExistentes(colaboradorDto, result);
		Colaborador colaborador = this.converterDtoParaColaborador(colaboradorDto, result);

		if (result.hasErrors()) {
			log.error("Erro validando dados do Colaborador: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}

		Optional<Setor> setor = this.setorService.buscarPorId(colaboradorDto.getSetorId());
		setor.ifPresent(set -> colaborador.setSetor(set));
		this.colaboradorService.persistir(colaborador);

		response.setData(this.converterColaboradorDto(colaborador));
		return ResponseEntity.ok(response);
	}	
	
	@GetMapping(value = "/colaboradores/id/{id}")
	public ResponseEntity<Response<ColaboradorDto>> listarPorId(@PathVariable("id") Long id) {
		
		log.info("Buscando colaborador: {}", id);

		Response<ColaboradorDto> response = new Response<ColaboradorDto>();
		Optional<Colaborador> colaborador = this.colaboradorService.buscarPorId(id);		
		
		if (!colaborador.isPresent()) {
			log.info("Colaborador não encontrado para o ID: {}", id);
			response.getErrors().add("Colaborador não encontrado para o id " + id);
			return ResponseEntity.badRequest().body(response);			
		}
		
		ColaboradorDto colaboradoresDto = this.converterColaboradorDto(colaborador.get());

		response.setData(colaboradoresDto);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping(value = "/colaboradores/cpf/{cpf}")
	public ResponseEntity<Response<ColaboradorDto>> listarPorCpf(@PathVariable("cpf") String cpf) {
		
		log.info("Buscando colaborador: {}", cpf);

		Response<ColaboradorDto> response = new Response<ColaboradorDto>();
		Optional<Colaborador> colaborador = this.colaboradorService.buscarPorCpf(cpf);		
		
		if (!colaborador.isPresent()) {
			log.info("Colaborador não encontrado para o CPF: {}", cpf);
			response.getErrors().add("Colaborador não encontrado para o cpf " + cpf);
			return ResponseEntity.badRequest().body(response);			
		}
		
		ColaboradorDto colaboradoresDto = this.converterColaboradorDto(colaborador.get());

		response.setData(colaboradoresDto);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping(value = "/colaboradores/email/{email}")
	public ResponseEntity<Response<ColaboradorDto>> listarPorEmail(@PathVariable("email") String email) {
		
		log.info("Buscando colaborador: {}", email);

		Response<ColaboradorDto> response = new Response<ColaboradorDto>();
		Optional<Colaborador> colaborador = this.colaboradorService.buscarPorEmail(email);		
		
		if (!colaborador.isPresent()) {
			log.info("Colaborador não encontrado para o email: {}", email);
			response.getErrors().add("Colaborador não encontrado para o email " +email);
			return ResponseEntity.badRequest().body(response);			
		}
		
		ColaboradorDto colaboradoresDto = this.converterColaboradorDto(colaborador.get());

		response.setData(colaboradoresDto);
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/colaboradores/{id}")
	public ResponseEntity<Response<String>> remover(@PathVariable("id") Long id) {
		
		log.info("Removendo colaborador: {}", id);
		Response<String> response = new Response<String>();
		Optional<Colaborador> colaborador = this.colaboradorService.buscarPorId(id);

		if (!colaborador.isPresent()) {
			log.info("Erro ao remover colaborador ID: {} ser inválido.", id);
			response.getErrors().add("Erro ao remover colaborador. Registro não encontrado para o id " + id);
			return ResponseEntity.badRequest().body(response);
		}

		this.colaboradorService.remover(id);
		return ResponseEntity.ok(new Response<String>());
	}

	private void validarDadosExistentes(ColaboradorDto colaboradorDto, BindingResult result) {
		
		if(colaboradorDto.getSetorId() == null) {
			result.addError(new ObjectError("colaborador", "Setor não pode ser vazio."));
		} else {		
			Optional<Setor> setor = this.setorService.buscarPorId(colaboradorDto.getSetorId());
			if (!setor.isPresent()) {
				result.addError(new ObjectError("colaborador", "Setor não cadastrado."));
			}
		}

		this.colaboradorService.buscarPorCpf(colaboradorDto.getCpf())
				.ifPresent(func -> result.addError(new ObjectError("colaborador", "CPF já existente.")));

		this.colaboradorService.buscarPorEmail(colaboradorDto.getEmail())
				.ifPresent(func -> result.addError(new ObjectError("colaborador", "Email já existente.")));
	}
	
	private Colaborador converterDtoParaColaborador(ColaboradorDto cadastroPFDto, BindingResult result)	throws NoSuchAlgorithmException {
		
		Colaborador colaborador = new Colaborador();
		colaborador.setCpf(cadastroPFDto.getCpf());	
		colaborador.setNome(cadastroPFDto.getNome());
		colaborador.setTelefone(cadastroPFDto.getTelefone());	
		colaborador.setEmail(cadastroPFDto.getEmail());

		return colaborador;
	}
	
	private ColaboradorDto converterColaboradorDto(Colaborador colaborador) {
		
		ColaboradorDto colaboradorDto = new ColaboradorDto();
		colaboradorDto.setId(colaborador.getId());
		colaboradorDto.setCpf(colaborador.getCpf());
		colaboradorDto.setNome(colaborador.getNome());
		colaboradorDto.setTelefone(colaborador.getTelefone());
		colaboradorDto.setEmail(colaborador.getEmail());
		colaboradorDto.setSetorId(colaborador.getSetor().getId());		
		
		return colaboradorDto;
	}

}
