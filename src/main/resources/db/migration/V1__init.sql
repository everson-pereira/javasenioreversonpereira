CREATE TABLE setor (
  id bigint(20) NOT NULL,
  descricao varchar(255) NOT NULL,  
);

CREATE TABLE colaborador (
  id bigint(20) NOT NULL,
  cpf varchar(255) NOT NULL,
  nome varchar(255) NOT NULL,
  telefone varchar(255) NOT NULL,
  email varchar(255) NOT NULL,  
  setor_id bigint(20) DEFAULT NULL
);

ALTER TABLE setor
  ADD PRIMARY KEY (id);

ALTER TABLE colaborador
  ADD PRIMARY KEY (id);

ALTER TABLE setor
  MODIFY id bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE colaborador
  MODIFY id bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE colaborador
  ADD CONSTRAINT FK_SETOR FOREIGN KEY (setor_id) REFERENCES setor (id);
