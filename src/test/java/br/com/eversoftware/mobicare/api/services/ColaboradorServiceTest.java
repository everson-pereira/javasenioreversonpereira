package br.com.eversoftware.mobicare.api.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.eversoftware.mobicare.api.entities.Colaborador;
import br.com.eversoftware.mobicare.api.repositories.ColaboradorRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ColaboradorServiceTest {

	@MockBean
	private ColaboradorRepository colaboradorRepository;

	@Autowired
	private ColaboradorService colaboradorService;

	@Before
	public void setUp() throws Exception {
		BDDMockito.given(this.colaboradorRepository.save(Mockito.any(Colaborador.class))).willReturn(new Colaborador());
		BDDMockito.given(this.colaboradorRepository.findById(Mockito.anyLong())).willReturn(Optional.ofNullable(new Colaborador()));
		BDDMockito.given(this.colaboradorRepository.findByEmail(Mockito.anyString())).willReturn(new Colaborador());
		BDDMockito.given(this.colaboradorRepository.findByCpf(Mockito.anyString())).willReturn(new Colaborador());
	}

	@Test
	public void testPersistirColaborador() {
		Colaborador colaborador = this.colaboradorService.persistir(new Colaborador());
		assertNotNull(colaborador);
	}

	@Test
	public void testBuscarColaboradorPorId() {
		Optional<Colaborador> colaborador = this.colaboradorService.buscarPorId(1L);
		assertTrue(colaborador.isPresent());
	}

	@Test
	public void testBuscarColaboradorPorEmail() {
		Optional<Colaborador> colaborador = this.colaboradorService.buscarPorEmail("email@email.com");
		assertTrue(colaborador.isPresent());
	}

	@Test
	public void testBuscarColaboradorPorCpf() {
		Optional<Colaborador> colaborador = this.colaboradorService.buscarPorCpf("65983135899");
		assertTrue(colaborador.isPresent());
	}

}
