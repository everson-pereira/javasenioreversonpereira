package br.com.eversoftware.mobicare.api.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.security.NoSuchAlgorithmException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.eversoftware.mobicare.api.entities.Colaborador;
import br.com.eversoftware.mobicare.api.entities.Setor;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ColaboradorRepositoryTest {

	@Autowired
	private ColaboradorRepository colaboradorRepository;

	@Autowired
	private SetorRepository setorRepository;

	private static final String EMAIL = "email@email.com";
	private static final String CPF = "55517461786";

	@Before
	public void setUp() throws Exception {
		Setor setor = this.setorRepository.save(getDadosSetor());
		this.colaboradorRepository.save(getDadosColaborador(setor));
	}

	@After
	public final void tearDown() {
		this.colaboradorRepository.deleteAll();
		this.setorRepository.deleteAll();
	}
	
	@Test
	public void testBuscarFuncionarioPorCpf() {
		Colaborador colaborador = this.colaboradorRepository.findByCpf(CPF);
		assertEquals(CPF, colaborador.getCpf());
	}

	@Test
	public void testBuscarFuncionarioPorEmail() {
		Colaborador colaborador = this.colaboradorRepository.findByEmail(EMAIL);
		assertEquals(EMAIL, colaborador.getEmail());
	}
	
	@Test
	public void testBuscarFuncionarioPorCpfInvalido() {
		Colaborador colaborador = this.colaboradorRepository.findByCpf("39282856704");
		assertNull(colaborador);
	}
	
	@Test
	public void testBuscarFuncionarioPorEmailInvalido() {
		Colaborador colaborador = this.colaboradorRepository.findByEmail("email@invalido.com");
		assertNull(colaborador);
	}	

	private Colaborador getDadosColaborador(Setor setor) throws NoSuchAlgorithmException {
		Colaborador colaborador = new Colaborador();
		colaborador.setNome("Fulano de Tal");
		colaborador.setCpf(CPF);
		colaborador.setTelefone("889953669");
		colaborador.setEmail(EMAIL);
		colaborador.setSetor(setor);
		return colaborador;
	}

	private Setor getDadosSetor() {
		Setor empresa = new Setor();
		empresa.setDescricao("setor1");		
		return empresa;
	}

}
