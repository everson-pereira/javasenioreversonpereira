package br.com.eversoftware.mobicare.api.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.eversoftware.mobicare.api.dtos.ColaboradorDto;
import br.com.eversoftware.mobicare.api.entities.Colaborador;
import br.com.eversoftware.mobicare.api.entities.Setor;
import br.com.eversoftware.mobicare.api.services.ColaboradorService;
import br.com.eversoftware.mobicare.api.services.SetorService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ColaboradorControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private ColaboradorService colaboradorService;
	
	@MockBean
	private SetorService setorService;
	
	private static final String URL_BASE = "/colaboradores/";
	private static final Long ID_COLABORADOR = Long.valueOf(1);
	private static final String CPF_COLABORADOR = "01346225532";
	private static final String NOME_COLABORADOR = "Éverson França Pereira";
	private static final String TELEFONE_COLABORADOR = "21997910369";
	private static final String EMAIL_COLABORADOR = "email@email.com";
	private static final Long ID_SETOR = Long.valueOf(1);
	
	
	@Test
	public void testCadastrarColaborador() throws Exception {
		Colaborador colaborador = obterDadosColaborador();
		BDDMockito.given(this.setorService.buscarPorId(Mockito.anyLong())).willReturn(Optional.of(new Setor()));
		BDDMockito.given(this.colaboradorService.persistir(Mockito.any(Colaborador.class))).willReturn(colaborador);

		mvc.perform(MockMvcRequestBuilders.post(URL_BASE)
				.content(this.obterJsonRequisicaoPost())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data.cpf").value(CPF_COLABORADOR))
				.andExpect(jsonPath("$.data.nome").value(NOME_COLABORADOR))
				.andExpect(jsonPath("$.data.telefone").value(TELEFONE_COLABORADOR))
				.andExpect(jsonPath("$.data.email").value(EMAIL_COLABORADOR))
				.andExpect(jsonPath("$.errors").isEmpty());
	}
	
	@Test
	public void testCadastrarColaboradorSetorIdInvalido() throws Exception {
		BDDMockito.given(this.setorService.buscarPorId(Mockito.anyLong())).willReturn(Optional.empty());

		mvc.perform(MockMvcRequestBuilders.post(URL_BASE)
				.content(this.obterJsonRequisicaoPost())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errors").value("Setor não cadastrado."))
				.andExpect(jsonPath("$.data").isEmpty());
	}
	
	@Test
	public void testRemoverColaborador() throws Exception {
		BDDMockito.given(this.colaboradorService.buscarPorId(Mockito.anyLong())).willReturn(Optional.of(new Colaborador()));

		mvc.perform(MockMvcRequestBuilders.delete(URL_BASE + ID_COLABORADOR)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	private String obterJsonRequisicaoPost() throws JsonProcessingException {
		ColaboradorDto lancamentoDto = new ColaboradorDto();
		lancamentoDto.setId(null);
		lancamentoDto.setCpf(CPF_COLABORADOR);
		lancamentoDto.setNome(NOME_COLABORADOR);
		lancamentoDto.setTelefone(TELEFONE_COLABORADOR);
		lancamentoDto.setEmail(EMAIL_COLABORADOR);
		lancamentoDto.setSetorId(ID_SETOR);
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(lancamentoDto);
	}

	private Colaborador obterDadosColaborador() {
		Colaborador colaborador = new Colaborador();
		colaborador.setId(ID_COLABORADOR);
		colaborador.setCpf(CPF_COLABORADOR);
		colaborador.setNome(NOME_COLABORADOR);
		colaborador.setNome(TELEFONE_COLABORADOR);
		colaborador.setNome(EMAIL_COLABORADOR);
		colaborador.setSetor(new Setor());
		colaborador.getSetor().setId(ID_SETOR);
		return colaborador;
	}	

}
