package br.com.eversoftware.mobicare.api.repositories;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.eversoftware.mobicare.api.entities.Setor;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SetorRepositoryTest {
	
	@Autowired
	private SetorRepository setorRepository;
	
	private static final Long id = 1L;
	private static final String descricao = "setor1";

	@Before
	public void setUp() throws Exception {
		Setor setor = new Setor();
		setor.setDescricao(descricao);		
		this.setorRepository.save(setor);
	}
	
	@After
    public final void tearDown() { 
		this.setorRepository.deleteAll();
	}

	@Test
	public void testBuscarPorCnpj() {
		Setor setor = this.setorRepository.findById(id).get();
		
		assertEquals(descricao, setor.getDescricao());
	}

}
