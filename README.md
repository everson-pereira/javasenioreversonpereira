# JavaSeniorEversonPereira

API processo seletivo Mobicare para cadastro de colaboradores

## Requisitos

1. Inserir colaborador (necessários para cadastrar um colaborador são: cpf, nome, telefone, e-mail).

2. Remover colaborador.

3. Buscar **UM** colaborador (campo de consulta nao informado, na dúvida utilizar os campos únicos (id, cpf, email)).

4. Listar colaboradores agrupados por setor (deverão ser exibidos os campos nome e e-mail dos colaboradores para cada setor).

5. Setores que já devem existir na tabela "setores" (possui um id e uma descrição).

6. Um colaborador deve pertencer somente a um dos setores

7. As tabelas devem ser criadas através de scripts.

8. Deve ser efetuada uma validação estrutural das tabelas existentes no banco no momento que a aplicação for iniciada.

## Critérios Técnicos

1. O projeto deve ser escrito em Java utilizando preferencialmente o Spring. 

2. É obrigatório a utilização do Maven para gerenciamento de dependências. 

3. Boas práticas de programação serão avaliadas. 

4. A lista de códigos de Status HTTP deve seguir a seguinte regra: 
	a. 201 - Recurso criado 
	b. 200 - Sucesso  
	c. 400 - Requisição inválida 
	d. 404 - Recurso não encontrado
	
## Linguagem

Utilização do Spring Framework

## Banco e Scripts SQL

Utilização do banco de Dados H2, os scripts para criação e população das tabelas (requisito 7) encontram-se na pasta `src/main/resources/db/migration`. O arquivo `V1\_\_init.sql` cria as tabelas ('Colaborador' e 'Setor') e o arquivo `V2\_\_setores` popula os setores (requisito 5). A configuração padrão utiliza o Flyway para o versionamento do banco porém, para cumprir o requisito de validação do banco (requisito 8) é necessario, APOS A PRIMEIRA EXECUÇÃO, alterar o arquivo `aplication.properties` na linha `spring.jpa.hibernate.ddl-auto = none` para `spring.jpa.hibernate.ddl-auto = validate`, pois a validação do Hibernate ocorre antes da criação das tabelas.

## Rodar a Aplicação

1. Importar a aplicação Maven para o STS

2. Executar a aplicação `Runs as > Spring Boot App`.

4. Com o banco criado e populado, deve-se parar a aplicação e voltar com a linha `spring.jpa.hibernate.ddl-auto = validate` para que na póroxima execução sua estrutura seja validada. A partir da nova execução os serviços podem ser manipulados como descritos na seção Serviços. Para tal pode ser usado o aplicativo Postman.

5. Os teste podem ser executados a qualquer momento, pois os mesmos usam o banco em memória definido no profile de testes e não interferem no banco da aplicação.
 
## Serviçoss
### Inserir Colaborador
####URL 
	POST /colaboradores
####Corpo
		"cpf" : "44774508349",
		"nome" : "Éverson França Pereira",
		"telefone" : "21997910369",
		"email" : "everson.pereirag@gmail.com",
		"setorId" : "1"
Os dados são obrigatórios (requisito 1). Na falta de algum deles uma mensagem de erro e o código de status correspondentes são enviados. No caso de inserção perfeita, os dados do colaborador e código de status são enviados. A inserção obrigatória do setor junto à validação de unicidade dos campos: cpf e email, garante a pertinência única de um colaborador à apenas um setor (requisito 6). Também são validados a corretude "estrutural "do CPF e email.
### Remover Colaborador
####URL
	DELETE /colaboradores/{id}
O parâmetro `{id}` indica o código do colaborador a ser excluído (requisito 2). Caso o mesmo não exista uma mensagem de erro e o código de status correspondentes são enviados. No caso de exclusão perfeita, código de status é enviados com resposta vazia.
### Buscar **UM** Colaborador
####URL
	GET /colaboradores/id/{id}'
	GET /colaboradores/cpf/{cpf}'
	GET /colaboradores/email/{email}'
Os parâmetros `{id}`, `{cpf}` e `{email}` indicam os dados pelos quais o colaborador deve ser buscado (requisito 3). Caso o mesmo não exista uma mensagem de erro e o código de status correspondentes são enviados. No caso de um colaborador ser encontrado, os dados do mesmo e código de status são enviados.
### Listar Colaboradores Agrupados por Setor
####URL
	GET /setores/colaboradores?pag=0&qt=1&ord=id&dir=ASC
Lista os setores com seus colaboradores (requisito 4) de forma paginada, ordenada e com cahe(2,5 minutos), seus parâmetros são: 

* `pag` = página,
* `qt`  = quantidade de setores por página,
* `ord` = campo a ser ordenado e
* `dir` = direção da ordenação ('ASC' ou 'DESC')

 

